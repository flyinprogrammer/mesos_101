# Setup the master

## exhibitor

```
#!/bin/bash
CLUSTER_NAME=ascherger
BUCKET=containerdays-exhibitor
HOSTNAME=$(hostname -f)

sudo tee /etc/docker/exhibitor <<-EOF
CONSUL_IP=localhost
DC=$CLUSTER_NAME
EXHIBITOR_HOSTNAME=$HOSTNAME
S3_BUCKET=$BUCKET
S3_PREFIX=$CLUSTER_NAME
EOF
sudo mkdir -p /opt/zk/transactions /opt/zk/snapshots
sudo chmod -R 777 /opt/zk 
sudo docker run --restart=always -d \
                --name=exhibitor \
                --net=host \
                --env-file=/etc/docker/exhibitor \
                -v /opt/zk/transactions:/opt/zk/transactions \
                -v /opt/zk/snapshots:/opt/zk/snapshots \
                flyinprogrammer/exhibitor
```

## mesos master

```
#!/bin/bash
CLUSTER_NAME=ascherger
IP_ADDRESS=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

sudo tee /etc/docker/mesos-master <<-EOF
CONSUL_IP=localhost
DC=$CLUSTER_NAME
MESOS_HOSTNAME_LOOKUP=false
MESOS_IP=$IP_ADDRESS
MESOS_ZK=zk://$CLUSTER_NAME.zookeeper.service.consul:2181/mesos
MESOS_PORT=5050
MESOS_LOG_DIR=/var/log/mesos
MESOS_QUORUM=1
MESOS_WORK_DIR=/var/lib/mesos
MESOS_CLUSTER=$CLUSTER_NAME
EOF
sudo docker run --restart=always -d --name=mesos-master --net=host --env-file=/etc/docker/mesos-master flyinprogrammer/mesos-master
```


## marathon

```
#!/bin/bash
CLUSTER_NAME=ascherger
PUBLIC_HOSTNAME=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)

sudo tee /etc/docker/marathon <<-EOF
CONSUL_IP=localhost
DC=$CLUSTER_NAME
MARATHON_MASTER=zk://$CLUSTER_NAME.zookeeper.service.consul:2181/mesos
MARATHON_ZK=zk://$CLUSTER_NAME.zookeeper.service.consul:2181/marathon
MARATHON_HOSTNAME=$PUBLIC_HOSTNAME
MARATHON_FRAMEWORK_NAME=$CLUSTER_NAME-marathon
EOF
sudo docker run --restart=always -d --name=marathon --net=host --env-file=/etc/docker/marathon flyinprogrammer/marathon
```